#!/usr/bin/python3

import pandas as pd
import numpy as np
import readline
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def moving_average(data_set, periods=7):
    weights = np.ones(periods) / periods
    return np.convolve(data_set, weights, mode='same')

markers = {
        'active' : 's',
        'confirmed' : 'x',
        'deaths' : 'o',
        'recovered' : 'D',
        'estimated positive' : 'P',
        }

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

my_colours = {}
for i,marker in enumerate(markers):
    my_colours[marker] = colors[i]
print("my_colours:", my_colours)

population = {
    'Czechia' : 10.58,
    'Austria' : 8.9,
    'Belgium' : 11.5,
    'Brazil' : 211,
    'China' : 1402,
    #'China' : 69, # 68M = population of Hubei province
    'Colombia' : 52.265,
    'France' : 67.0,
    'Germany' : 83.1,
    'India' : 1360,
    'Iran' : 83.3,
    'Israel' : 9.18,
    'Italy' : 60.2,
    'Japan' : 126.0,
    'Korea, South' : 51.58,
    'Netherlands' : 17.5,
    'Peru' : 32,
    'Russia' : 146,
    'Slovakia' : 5.45,
    'Spain' : 47.1,
    'Sweden' : 10.1,
    'Turkey' : 83.1,
    'Poland' : 38.4,
    'United Kingdom' : 66.4,
    'US' : 239.5,
        }

countries_to_plot = [
    'Czechia',
    'Austria',
    'Belgium',
    'Brazil',
    'China',
    'Colombia',
    'France',
    'Germany',
    'India',
    'Iran',
    'Israel',
    'Italy',
    'Japan',
    'Korea, South',
    'Netherlands',
    'Peru',
    'Poland',
    'Russia',
    'Slovakia',
    'Spain',
    'Sweden',
    'Turkey',
    'US',
    'United Kingdom',
        ]

#countries_to_plot = [
#    'France',
#    'Italy',
###    'Czechia',
#        ]

first_entry=4;
est_threshold = 100. # estimated threshold in number of deaths per 1M until the healthcare system collapses
death_fac = 1e-2 # estimated fraction of infected people who die
days_till_death=12 # estimated number of days between infection and deaths

file_path = 'COVID-19/csse_covid_19_data/csse_covid_19_time_series/'

# common settings for plt
plt.rc("font", family='sans', size=12)

#for country in countries_to_plot:
#    print(country)
#    with PdfPages(country+'.pdf') as pdf:
with PdfPages('all.pdf') as pdf:
    for country in countries_to_plot:
        print(country)
        cases = {}
        # obtain the data
        case_types=['active','confirmed', 'recovered', 'deaths']
        for case_type in case_types:
            if (case_type != 'active'):
                fn=file_path+'time_series_covid19_'+case_type+'_global.csv'
                df = pd.read_csv(fn) # read dataframe 
                values = df[ df["Country/Region"]==country ]
                #print("len:", values.shape, values.T[first_entry:].shape)
                new_values = values.T.values[first_entry:]
                avg_values = np.sum(new_values,axis=1)
                #print(new_values, avg_values)
                cases[case_type] = avg_values
        cases['active'] = cases['confirmed'] - cases['recovered'] - cases['deaths']
        choicelist = np.array(cases['deaths'])
        # (arbitrary) threshold for estimating: 100 cases per 1M inhabitants = health care has no yet collapsed
        condlist = np.array(cases['deaths']/population[country] < est_threshold) * np.array(cases['deaths'] > 10.0)
        cases['estimated positive'] = condlist*choicelist/death_fac
        
        # all case types in one plot
        fig,ax =plt.subplots(constrained_layout=True)
        plt.grid(linestyle='--',linewidth=0.5)
        ax.set_xlabel('days')
        ax.set_yscale("log")
        ax.set_ylim(1e-2,1e6)
        ax.set_ylabel('cases per 1M inhabitants')
        for case_type in case_types:
            new_values = cases[case_type] 
            #print(case_type,new_values)

            days = np.arange(0,len(new_values))
            ax.plot(
                days,
                new_values/population[country],
                label = case_type,
                linestyle="--",
                markersize=5.0,
                marker = markers[case_type],
                fillstyle = 'none',
                alpha = 0.8,
            )
            #print(case_type)
        case_type='estimated positive'
        new_values = cases[case_type] /population[country]
        days = np.arange(0,len(new_values))
        ax.plot(
            days-days_till_death,
            new_values,
            label = case_type,
            linestyle="none",
            markersize=5.0,
            marker = markers[case_type],
            fillstyle = 'none',
            alpha = 0.8,
        )
        ax.legend(
            title = country,
            ncol=1,
            fontsize="small",
            loc="upper left",
        )
        pdf.savefig(bbox_inches="tight")
        plt.close()
    
        # daily increase in all case types in one plot
        fig,ax = plt.subplots(constrained_layout=True)
        plt.grid(linestyle='--',linewidth=0.5)
        ax.set_xlabel('days')
        ax.set_yscale("log")
        ax.set_ylim(2e-3,1e3)
        ax.set_ylabel('New cases per 1M inhabitants')
        for case_type in case_types:
            new_values = cases[case_type] 
            days = np.arange(0,len(new_values))
            diff = (new_values[1:] - new_values[:-1])/population[country]
            ax.plot(
                days[1:],
                diff,
                label = case_type,
                linestyle="",
                markersize=2.0,
                marker = markers[case_type],
                color = my_colours[case_type],
                fillstyle = 'none',
                alpha = 0.8,
            )
            #print("diff:", diff)
            avg = moving_average(diff)
            ax.plot(
                days[1:-3],
                avg[:-3],
                linestyle="",
                markersize=5.0,
                marker = markers[case_type],
                color = my_colours[case_type],
                fillstyle = 'none',
                alpha = 0.8,
            )
            #print(case_type)
        ax.legend(
            title = country,
            ncol=1,
            fontsize="small",
            loc="upper left",
        )
        pdf.savefig(bbox_inches="tight")
        plt.close()
        # linear scale daily increase in all case types in one plot
        fig,ax = plt.subplots(constrained_layout=True)
        plt.grid(linestyle='--',linewidth=0.5)
        ax.set_xlabel('days')
        #ax.set_ylim(-50,120)
        ax.set_ylabel('New cases per 1M inhabitants')
        for case_type in case_types:
            new_values = cases[case_type] 
            days = np.arange(0,len(new_values))
            diff = (new_values[1:] - new_values[:-1])/population[country]
            ax.plot(
                days[1:],
                diff,
                label = case_type,
                linestyle="",
                markersize=2.0,
                marker = markers[case_type],
                color = my_colours[case_type],
                fillstyle = 'none',
                alpha = 0.8,
            )
            avg = moving_average(diff)
            ax.plot(
                days[1:-3],
                avg[:-3],
                linestyle="",
                markersize=5.0,
                marker = markers[case_type],
                color = my_colours[case_type],
                fillstyle = 'none',
                alpha = 0.8,
             )
            #print(case_type)
        ax.legend(
            title = country,
            ncol=1,
            fontsize="small",
            loc="upper left",
        )
        pdf.savefig(bbox_inches="tight")
        plt.close()

