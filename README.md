# covid-19

Plotting and rudimentary analysis of the covid-19 data by Peter Košovan

Enjoy and use at your own risk.

To get the script working, you need to add a submodule to fetch the data from Johns Hopkins covid data centre:

git submodule add https://github.com/CSSEGISandData/COVID-19.git

